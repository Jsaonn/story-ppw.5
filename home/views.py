from django.shortcuts import render

def home(request):
	return render(request, 'home.html')

def project(request):
	return render(request, 'project.html')

def story1(request):
	return render(request, 'story1.html')

def matkul(request):
	return render(request, 'index.html')