from django.urls import path
from . import views

app_name = "home"

urlpatterns = [
	path('', views.home, name='home'),
    path('/project', views.project, name='project'),
    path('/story', views.story1, name='story'),
    path('/matkul', views.matkul, name='matkul')
]
