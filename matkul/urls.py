from django.urls import path
from . import views

appname = 'matkul'

urlpatterns = [
    path('', views.jadwal),
    path('<int:pk>/', views.delete, name = 'hapus'),
    
]