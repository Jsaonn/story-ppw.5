# Generated by Django 3.0 on 2020-10-15 03:10

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='matkul',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama_mata_kuliah', models.CharField(max_length=100)),
                ('nama_dosen', models.CharField(max_length=100)),
                ('jumlah_sks', models.CharField(max_length=3)),
                ('deskripsi_mata_kuliah', models.CharField(max_length=200)),
                ('tahun_semester', models.CharField(max_length=50)),
                ('ruang_kelas', models.CharField(max_length=100)),
            ],
        ),
    ]
