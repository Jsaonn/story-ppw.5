from django.db import models

class Matkul(models.Model):
    nama_mata_kuliah = models.CharField(blank=False, max_length = 100)
    nama_dosen = models.CharField(blank=False, max_length=100)
    jumlah_sks = models.CharField(blank=False, max_length = 3)
    deskripsi_mata_kuliah = models.CharField(blank=False, max_length = 200)
    tahun_semester = models.CharField(blank=False, max_length = 50)
    ruang_kelas = models.CharField(blank=False, max_length = 100)
